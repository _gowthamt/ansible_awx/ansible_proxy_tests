---
###############################################################################
#
#
# Copyright 2022, Cisco Systems, Inc.
# All Rights Reserved.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#
# SDWAN Proxy Testing - http and sdwan collections tasks
#
# Created by: Gowtham Tamilselvan
# Revision Number: 1.2
# Last Updated: Feb 17 2022
# Cisco Systems, Inc./Sdwan SRE TEAM
#
# Cisco Product tested : vManage
# Cisco Software Version tested : 20.4.1
# Created using Ansible version 2.9.21, minimum version 2.9 required.
# 
# Sample Script Execution : ansible-playbook 03_http_collections_test.yml -e "ansible_network_os=ios" -e "sre_vmanage=vManage301" -e "ansible_python_interpreter=/usr/local/bin/python3.9"
#
# Tool requires jmespath to be installed "pip3 install jmespath" inorder to for Jmesquery to work.
#
###############################################################################
- hosts: "{{ sre_vmanage | default('vManage302')}}"
  gather_facts: no
  #remote_user: "{{ remoteuser }}"
  #remote_user: cisco
  #connection: network_cli

  pre_tasks:
    - name: Verify Ansible meets version requirements
      ansible.builtin.assert:
        that: "ansible_version.full is version_compare('2.6', '>=')"
        msg: >
          "You must update Ansible to at least 2.6 to use this script."
      tags: always
    
    - name: Print Cisco Systems EULA 
      vars:
        msg: |
          "Cisco Systems, Inc End User License Agreement 2020. 

          As a general rule, software is not included in our Services and must be purchased separately. For Technical Support Services
          that expressly include Software Updates, your right to use the Software is covered under your original license. For any other 
          Software and Scripts that Cisco provides as part of the Services, it is provided as a convenience to you and incidental to the
          provision of Services (“Incidental Software and Scripts”).

          All Incidental Software and Scripts, in whatever form provided, are licensed to you solely for the express purposes of the
          Services and in accordance with our EULA located at http://www.cisco.com/go/eula. If we provide you with Source Code for
          any Incidental Software and Scripts, then the Source Code, including any copies, modifications, enhancements and derivative
          works of the Source Code, is Cisco Confidential Information that you must keep secure with access given only to your
          personnel who must access the Source Code to accomplish the purposes of the Services. Unless we state otherwise, the
          Source Code license we grant to you for Incidental Software and Scripts includes the limited license to modify and enhance
          the provided Source Code solely for your internal use and only to the extent we expressly permit. Upon our request, you must
          remove and substitute, or allow us to remove and substitute, this Source Code with functionally equivalent object code,
          provided the object code substitution will only occur if you continue to have an applicable license to the Incidental Software
          and Scripts.

          Disclaimer of Warranty and Support: Your warranties for the overall Service are provided in your Master Agreement. For
          Incidental Software and Scripts, except as we otherwise expressly grant, these items are provided “AS-IS,” “With All Faults,”
          and without warranties) of any kind (whether they are express, implied, or statutory). We have no obligations with respect
          to support or maintenance, including without limitation, upgrades, updates, maintenance releases, or modifications, of the
          Incidental Software and Scripts"
      ansible.builtin.debug:
        msg: "{{ msg.split('\n') }}"
      tags: never

  tasks:
    - name: Initial Empty array
      ansible.builtin.set_fact: 
        ds_hostname: []
        ds_devicetype: []
        ds_systemip: []
        ds_softwareversion: []
        ds_certificatevalidity: []
        cc_peertype: []
        cc_peerstate: []
        cc_peeruptime: []
        cc_peerip: []
        cc_len: []
        dc_systemip: []
        dc_vsmart_connections: []
        dc_expected_connections: []
        nms_systemip: []
        nms_cloudAgent: []
        nms_ApplicationServer: []
        nms_StatisticsDB: []
        nms_ConfigurationDB: []
        nms_MessagingServer: []
        nms_SDAVC: []

#    - name: CLI Check vManage version
#      cisco.ios.ios_command:
#        commands: "show version | nomore"
#      register: OUTPUT

#    - name: CLI print output
#      debug:
#        var: OUTPUT.stdout_lines
      
    # Need to extract vManage System IP from vars file based on inventory_hostname
    # Should look like this - vManage.vManage101.system_ip
    - name: Dynamically Get vmanage systetreemip from vars file
      ansible.builtin.set_fact: vmanagesystemip="{{ item.value.system_ip }}"
      with_dict:
      #with_items:
        - "{{ vManage }}"
      when: item.key == inventory_hostname

    #- name: Dummy Task
    #  raw: "pwd"
    #  delegate_to: "{{ remotejh }}"


    # vManage API Authentication 
    - name: API Authentication and get cookie
      ansible.builtin.include_tasks: get-token.yml
      when: viptela_api_cookie is not defined


    #[1] vManage API - Get Device Stats
    - name: 1 - API GET Device Stats
      ansible.builtin.uri:
        url: https://{{ansible_host}}:8443/dataservice/device
        method: GET
        return_content: yes
        validate_certs: no
        headers:
          Cookie: "{{ viptela_api_cookie }}"
          Content-Type: "application/json"
          X-XSRF-TOKEN: "{{ viptela_api_token }}"
        #use_proxy: false
      register: device_stats
      #environment: "{{ proxy_env }}"
      delegate_to: "{{ remotejh }}"
      remote_user: "{{ remoteuser }}"
      #connection: local

    #[1] Parse the output to extract device data
    - name: 1 - Parse Device Stats Data Set
      ansible.builtin.set_fact:
        ds_hostname: "{{ ds_hostname + [ item.HostName ] }}"
        ds_devicetype: "{{ ds_devicetype + [ item.Type ] }}"
        ds_systemip: "{{ ds_systemip + [ item.SystemIp ] }}"
        ds_softwareversion: "{{ ds_softwareversion + [ item.SoftwareVersion ] }}"
        ds_certificatevalidity: "{{ ds_certificatevalidity + [ item.CertificateValidity ] }}"
        ds_len: "{{  ds_hostname|length }}"
      loop: "{{ device_stats.json | json_query(jmesquery) }}"
      vars:
        jmesquery: 'data[*].{HostName: "host-name", Type: "device-type", SystemIp: "system-ip", SoftwareVersion: version, CertificateValidity: "certificate-validity"}'
      #no_log: True


    #[1] Conditional check for certificate validity
    - name: 1 - API Check Device Stats
      ansible.builtin.debug: msg=" Hostname {{ ds_hostname[item|int] }}, Device-type {{ ds_devicetype[item|int] }}, System-IP {{ ds_systemip[item|int] }}, Software Version {{ ds_softwareversion[item|int] }}, Certificate Validity {{ ds_certificatevalidity[item|int] }}"
      with_sequence: start=0 end="{{ ds_len|int }}"
      failed_when: '"Valid" not in ds_certificatevalidity'
      ignore_errors: True

    #[2] SDWAN Collections API - REALTIME Collection
    - name: Show realtime
      cisco.sdwan.show_realtime:
        address: "{{ ansible_host }}"
        user: "{{ deviceuser }}"
        password: "{{ ansible_password }}"
        cmd: ['system']
        system_ip: "100.1.3.4"
      delegate_to: "{{ remotejh }}"
      remote_user: "{{ remoteuser }}"
      register: result

    - name: Print Realtime
      debug:
        msg: "{{ item }}"
      with_items: "{{ result.tables }}"


    #[2] SDWAN Collections API - Inventory Collection
    - name: Show realtime
      cisco.sdwan.inventory:
        address: "{{ ansible_host }}"
        user: "{{ deviceuser }}"
        password: "{{ ansible_password }}"
        regex_list:
          - "vManage301"
        device_type: "vmanage"
      delegate_to: "{{ remotejh }}"
      remote_user: "{{ remoteuser }}"
      register: inventory

    - name: Print Inventory
      debug:
        msg: "{{ item }}"
      with_items: "{{ inventory.json }}"




