# **<p align="center">Ansible Proxy Testing</p>**

### **<p align="center">Gowtham Tamilselvan </p>**
### **<p align="center">Feb 17 2022</p>**

---

## Overview

Set of ansible playbooks to test SSH (CLI module), HTTP, and SDWAN Collections execution through a Proxy server.

```
├── 01_http_vmanage_monitor.yml
├── 01_sdwan_collections_inventory.yml
├── 01_sdwan_collections_realtime.yml
├── 01_ssh_vmanage_cli.yml
├── 02_ssh_http_test.yml
├── 03_http_collections_test.yml
├── README.md
├── ansible.cfg
├── get-token.yml
├── group_vars
│   └── all.yml
├── hosts
└── vmanage_cli.yml

1 directory, 12 files
```

## Setup Overview

LocalMachine ---------- Jumpserver/DomainServer ---------- Network Device

LocalMachine - location where ansible playbooks are executed, contains ansible, python, and collections are installed

Jumpserver - required ansible, python, and collections should be installed

NetworkDevice - only reachable from Jumpserver, not directly reachable from LocalMachine

## Release Date
2022-02-17

## Usage

- ansible-playbook 01_ssh_vmanage_cli.yml -e "ansible_network_os=ios" -e "sre_vmanage=vManage301" 
- ansible-playbook 01_http_vmanage_monitor.yml -e "ansible_network_os=ios" -e "sre_vmanage=vManage301"
- ansible-playbook 01_sdwan_collections_inventory.yml -e "ansible_network_os=ios" -e "sre_vmanage=vManage301" -e "ansible_python_interpreter=/usr/local/bin/python3.9"
- ansible-playbook 01_sdwan_collections_realtime.yml -e "ansible_network_os=ios" -e "sre_vmanage=vManage301" -e "ansible_python_interpreter=/usr/local/bin/python3.9"
- ansible-playbook 02_ssh_http_test.yml -e "ansible_network_os=ios" -e "sre_vmanage=vManage301"
- ansible-playbook 03_http_collections_test.yml -e "ansible_network_os=ios" -e "sre_vmanage=vManage301" -e "ansible_python_interpreter=/usr/local/bin/python3.9"

